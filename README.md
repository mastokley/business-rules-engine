# rules-engine

This is a proof of concept implementation of a business rules engine in Clojure.

Each business rule is like a partial function - it has an incomplete domain.
Each business rule also has a priority. Given a set of rules and some context,
the rules engine returns the highest-priority rule that is applicable. It is
then up to the consumer of the rules engine to determine the significance of the
matched rule.

The rules are written in json in a domain specific language. Since the language
is so simple, it's easy for folks to create, update, or remove rules without
being familiar with Clojure.

## Why Clojure?

The business rules are data that need to be expressed as code, somehow. Lisps,
including Clojure, excel at treating data as code and vice versa.

Furthermore, the rules already resemble lisp code: an expression tree with
prefix notation.

The rules engine

1. consumes json rules,
2. transforms the json into Clojure code,
3. and then evaluates it

The trick is to use a language feature called "syntax quoting". Syntax quoting
tells Clojure not to evaluate a given "form". It's a way of treating code as
data. The rules engine uses syntax quoting to build Clojure expressions given
the raw data of the rules.

For example, this rule

```json
{
    "and": [{
        "condition": {
            "variable": "REQUEST_URL",
            "operator": "CONTAINS",
            "value": "utm_medium=email"
        }
    }, {
        "condition": {
            "variable": "REQUEST_URL",
            "operator": "CONTAINS",
            "value": "utm_source=sfmc"
        }
    }]
}
```

would be transformed into (approximately) this Clojure expression:

```clojure
(rules-engine-and (contains request_url "utm_medium=email")
                  (contains request_url "utm_source=sfmc"))
```

and then evaluated with appropriate values for `rules-engine-and`, `contains`,
and `request_url`.

For Clojure, this is merely an exercise in data transformation. We avoid needing
to implement a DSL interpreter.
