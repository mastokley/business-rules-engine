(ns rules-engine.core
  (:require [clojure.java.io :as io]
            [clojure.spec.alpha :as spec]
            [clojure.walk :as w]
            [cheshire.core]))

(defn rules
  [rules-file]
  (-> rules-file
      io/resource
      io/file
      slurp
      cheshire.core/parse-string
      (get "body")))

(defn map-into-list*
  "Prepare the raw json for evaluation by transforming into lists.

  leaf node:
      {k1 v1, k2 v2}         => {k1 v1, k2 v2}

  node with vector child:
      {k [{k1 v1} {k2 v2}]}  => (k [(k1 v1) (k2 v2)])

  node with scalar child:
      {k v}                  => (k v)
      {k {k1 v1, k2 v2}}     => (k {k1 v1, k2 v2})
      {k {k v}}              => (k {k v})"

  [m]
  (let [leaf? #(> (count (keys %)) 1)
        operator (first (keys m))
        operand (first (vals m))]
    ;; NOTE order matters! conditions not mutually exclusive
    (cond (leaf? m) m
          (sequential? operand) (list operator
                                      (mapv map-into-list* operand))
          :else (list operator operand))))

(defn is-applicable?
  "Determine whether a given rule applies to provided variables."
  [operators]
  (fn
    [rule variables]
    (let [replace-strings* #(w/postwalk-replace (merge operators variables) %)
          build-expression-tree (comp map-into-list* replace-strings*)]
      (-> rule
          (get-in ["definition" "logic"])
          build-expression-tree
          eval))))

(defn first-matching-rule
  [operators]
  (fn
    [rules variables]
    (let [is-applicable? #((is-applicable? operators) % variables)]
      (reduce
       (fn [prev-best cur]
         (if (and (< (get cur "executionOrder")
                     (get prev-best "executionOrder" Long/MAX_VALUE))
                  (is-applicable? cur))
           cur
           prev-best))
       nil
       rules))))

(defn -main
  [& args])
