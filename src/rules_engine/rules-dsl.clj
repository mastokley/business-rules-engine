(ns rules-engine.rules-dsl
  (:require [clojure.string :as string]))

(def operators
  {"or"               `(comp boolean (partial some identity)),
   "and"              `(comp boolean (partial every? identity)),
   "EQUALS"           `=,
   "CONTAINS"         `string/includes?,
   "STARTS_WITH"      `string/starts-with?,
   "DOES_NOT_CONTAIN" `(comp not string/includes?),
   ;; use syntax unquote (`~`) and quote (`'`) to avoid namespace-qualifying
   ;; internal function bindings
   "condition"        `(fn [{:strs ~'[variable operator value]}]
                         ~'(operator variable value))})
