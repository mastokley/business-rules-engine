(ns rules-engine.core-test-data
  (:require [rules-engine.core :as sut]
            [clojure.test :as t]))

(def conditions
  [{"condition" {"variable" "REQUEST_URL",
                 "operator" "CONTAINS",
                 "value" "consumerreports.org"}}
   {"condition" {"variable" "REQUEST_URL",
                 "operator" "CONTAINS",
                 "value" "utm_medium=cpc"}}
   {"condition" {"variable" "REQUEST_URL",
                 "operator" "CONTAINS",
                 "value" "utm_source=bing"}}
   {"condition" {"variable" "REFERRER_URL",
                 "operator" "CONTAINS",
                 "value" "tracking.bigcorp.com/track"}}
   {"condition" {"variable" "REQUEST_URL",
                 "operator" "CONTAINS",
                 "value" "utm_medium=email"}}
   {"condition" {"variable" "REQUEST_URL",
                 "operator" "CONTAINS",
                 "value" "utm_source=sv"}}
   {"condition" {"variable" "REQUEST_URL",
                 "operator" "CONTAINS",
                 "value" "utm_source=strongview"}}])

(def variables
  {:satisfies-no-rules
   {"REQUEST_URL" "",
    "REFERRER_URL" ""}
   :satisfies-first-rule
   {"REFERRER_URL" "",
    "REQUEST_URL" "consumerreports.org"}
   :satisfies-second-rule
   {"REFERRER_URL" "",
    "REQUEST_URL" "utm_medium=cpc&utm_source=bing"}
   :satisfies-first-and-second-rule
   {"REFERRER_URL" "",
    "REQUEST_URL" "consumerreports.org?utm_medium=cpc&utm_source=bing"}
   :satisfies-third-rule
   {"REQUEST_URL" "",
    "REFERRER_URL" "tracking.bigcorp.com/track?utm_medium=email"}})

(def rules
  [{"executionOrder" 0
    "definition" {"logic" (nth conditions 0)}}

   {"executionOrder" 1
    "definition" {"logic" {"and"
                           [(nth conditions 1) (nth conditions 2)]}}}
   {"executionOrder" 2
    "definition" {"logic" {"or"
                           [(nth conditions 3)
                            {"and"
                             [(nth conditions 4)
                              {"or"
                               [(nth conditions 5)
                                (nth conditions 6)]}]}]}}}])
