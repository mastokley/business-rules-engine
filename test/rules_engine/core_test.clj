(ns rules-engine.core-test
  (:require [clojure.test :refer :all]
            [rules-engine.core-test-data :as data]
            [rules-engine.core :refer :all]
            [rules-engine.rules-dsl :as rdsl]))

(deftest test-map-into-list*
  (is (= '(k {k1 v1, k2 v2})
         (map-into-list* '{k {k1 v1, k2 v2}}))
      "child, which is a map")
  (is (= '(f ((k v) (g ((k v) (k v)))))
         (map-into-list* '{f [{k v} {g [{k v} {k v}]}]}))
      "jagged")
  (is (= '(k ((k v) (k v)))
         (map-into-list* '{k [{k v} {k v}]}))
      "recurses into nested maps")
  (is (= '(k v)
         (map-into-list* '{k v}))
      "unless it knows that the value is a many-keyed map, returns list")
  (is (= '{k1 v1, k2 v2}
         (map-into-list* '{k1 v1, k2 v2}))
      "leaf"))

(deftest test-is-applicable?
  (let [is-applicable? (is-applicable? rdsl/operators)]
  (testing "evaluating a single-condition rule"
    (is (true? (is-applicable? (first data/rules)
                              (:satisfies-first-rule data/variables)))
        "single condition is true")
    (is (false? (is-applicable? (first data/rules)
                               {"REFERRER_URL" "",
                                "REQUEST_URL" ""}))
        "single condition is false"))
  (testing "evaluating multiple conditions joined by `and`"
    (is (true? (is-applicable? (second data/rules)
                              (:satisfies-second-rule data/variables)))
        "two conditions; with booleans; true")
    (is (false? (is-applicable? (first data/rules)
                              (:satisfies-second-rule data/variables)))
        "two conditions; with booleans; true")
    (is (false? (is-applicable? (second data/rules)
                               {"REFERRER_URL" "",
                                "REQUEST_URL" "utm_medium=cpc"}))
        "two conditions; with booleans; false"))
  (testing "evaluating a nested, complex boolean expression with many conditions"
    (let [rule (nth data/rules 2)]
      (is (true? (is-applicable? rule
                                {"REFERRER_URL" "tracking.bigcorp.com/track"
                                 "REQUEST_URL" ""}))
          "(or true (and false (or false false))")
      (is (true? (is-applicable? rule
                                {"REFERRER_URL" ""
                                 "REQUEST_URL" "utm_medium=email&utm_source=sv"}))
          "(or false (and true (or true false)))")
      (is (true? (is-applicable? rule
                                {"REFERRER_URL" ""
                                 "REQUEST_URL" "utm_medium=email&utm_source=strongview"}))
          "(or false (and true (or false true)))")
      (is (false? (is-applicable? rule
                                 {"REFERRER_URL" ""
                                  "REQUEST_URL" "utm_source=strongview"}))
          "(or false (and false (or false true)))")
      (is (false? (is-applicable? rule
                                 {"REFERRER_URL" ""
                                  "REQUEST_URL" "utm_medium=email"}))
          "(or false (and true (or false false)))")))))

(deftest test-first-matching-rule
  (let [first-matching-rule (first-matching-rule rdsl/operators)]
    (is (= (first data/rules)
           (first-matching-rule
            data/rules
            (:satisfies-first-rule data/variables)))
        "only first listed and highest priority rule matches")
    (is (= (second data/rules)
           (first-matching-rule
            data/rules
            (:satisfies-second-rule data/variables)))
        "only second listed and second priority rule matches")
    (is (= (first data/rules)
           (first-matching-rule
            data/rules
            (:satisfies-first-and-second-rule data/variables)))
        "of two matching rules, return highest priority rule")
    (is (= nil
           (first-matching-rule
            data/rules
            (:satisfies-no-rules data/variables)))
        "no rules matched")
    (is (= (nth data/rules 2)
           (first-matching-rule
            data/rules
            (:satisfies-third-rule data/variables)))
        "third rule is nested and complicated")))

